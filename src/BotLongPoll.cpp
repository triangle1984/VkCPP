#include "VkCPP/types.h"
#include "VkCPP/vk.h"
#include <iostream>
#include <string>
namespace VkCPP {
json VK::GetBotLongPoll() {
  return call("groups.getLongPollServer", {{"group_id", group_id}})["response"];
}
void VK::SetBotLongPoll(string group_id) {
  this->group_id = group_id;
  auto res = GetBotLongPoll();
  this->server = res["server"];
  this->key = res["key"];
}
json VK::BotPoll() {
  static string ts = "0";
  if (ts == "0") {
    ts = to_string(GetBotLongPoll()["ts"]);
  }
  auto res = VK::request(
      server, {{"key", key}, {"act", "a_check"}, {"wait", "25"}, {"ts", ts}});
  ts = res["ts"];
  return res["updates"][0]["object"];
}
} // namespace VkCPP
