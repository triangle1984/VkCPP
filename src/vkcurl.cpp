#include "VkCPP/types.h"
#include "VkCPP/vk.h"
#include <curl/curl.h>
namespace VkCPP {
string VK::urlencode(string url) {
  string urlen;
  for (char c : url) {
    if (c == ' ') {
      urlen += "%20";
    } else {
      urlen += c;
    }
  }
  return urlen;
}
int VK::writer(char *data, size_t size, size_t nmemb, string *buffer) {
  int result = 0;
  if (buffer != NULL) {
    buffer->append(data);
    result = size * nmemb;
  }
  return result;
}
string VK::backrequests(char *url) {
  CURL *curl;
  curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
    curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
  return buffer;
}
string VK::maptourl(args args) {
  string url = "?";
  for (auto args : args) {
    url += args.first + "=" + urlencode(args.second) + "&";
  }
  return url;
}
json VK::request(string url, args args) {
  url.append(maptourl(args));
  char *url2 = &url[0]; // string to char, курл пидарас. FIXME
  return jsonhandler(backrequests(url2));
}
} // namespace VkCPP
