#include "VkCPP/vk.h"
#include "VkCPP/types.h"
namespace VkCPP {
VK::VK(string token) { this->token = token; }

json VK::call(string method, args args) {
  args.insert({{"v", "5.52"}, {"access_token", this->token}});
  return request(apiurl + method, args);
}
} // namespace VkCPP
