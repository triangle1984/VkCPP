#include "VkCPP/types.h"
#include "VkCPP/vk.h"
namespace VkCPP {
json VK::jsonhandler(string text) {
  buffer.clear();
  return json::parse(text);
}
} // namespace VkCPP
