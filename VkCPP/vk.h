#include "VkCPP/types.h"
#include "lib/json.hpp"
namespace VkCPP {

class VK {
  // curl
  string buffer;
  string urlencode(string url);
  static int writer(char *data, size_t size, size_t nmemb, string *buffer);
  string maptourl(args args);
  string backrequests(char *url);
  json jsonhandler(string text);
  json request(string url, args args);
  json GetBotLongPoll();

public:
  string token;
  string server, key, group_id;
  string version = "5.52";
  VK(string token);
  void SetBotLongPoll(string group_id);
  json BotPoll();
  json call(string method, args args);
};
} // namespace VkCPP
